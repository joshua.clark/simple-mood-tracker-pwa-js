import React, {Component} from 'react';
import routes from './routes.js';
import {Provider} from 'react-redux';
import {createStore} from 'redux';
import rootReducer from './reducers/index';
import {HashRouter} from 'react-router-dom';
import {MuiThemeProvider, createMuiTheme} from "@material-ui/core/es/styles/";
import Signin from './components/Signin.jsx';
import MainAppBar from "./components/MainAppBar.jsx";
import {
    isSignInPending,
    isUserSignedIn,
    redirectToSignIn,
    handlePendingSignIn,
    signUserOut,
} from 'blockstack';
const theme = createMuiTheme();
//Initialize the redux global state or store
const store = createStore(rootReducer);



class App extends Component {

    constructor(props) {
        super(props);
    }

    handleSignIn(e) {
        e.preventDefault();
        const origin = window.location.origin;
        redirectToSignIn(origin, origin + '/manifest.json', ['store_write', 'publish_data'])
    }

    handleSignOut(e) {
        e.preventDefault();
        signUserOut(window.location.origin);
    }

    render() {
        return (
            <div>
                {!isUserSignedIn() ? <Signin handleSignIn={this.handleSignIn}/> :
                    <MuiThemeProvider theme={theme}>
                        <Provider store={store}>
                            <HashRouter>
                                <div style = {{
                                    overflowX: "hidden",
                                    maxWidth: "100%"
                                }}>
                                    <MainAppBar handleSignOut={this.handleSignOut}/>
                                    {routes}
                                </div>
                            </HashRouter>
                        </Provider>
                    </MuiThemeProvider>
                }
            </div>
        );
    }

    componentWillMount() {
        if (isSignInPending()) {
            handlePendingSignIn().then((userData) => {
                window.location = window.location.origin;
            });
        }
    }
}

export default App;
