import React from "react";
import WmtpSlider from "wmtp-slider";
import WmtpResultsGraph from 'wmtp-graph/lib/'
import Button from "@material-ui/core/Button";
import {
    getFile,
    putFile
} from 'blockstack';


class Home extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            scores: [],
            depressionSlider: 50,
            anxietySlider: 50
        };
        this.loadGraphDataFromGaiaStorage();
    }


    /**
     * Load the users score data into the graph
     */
    loadGraphDataFromGaiaStorage = () => {
        let options = {decrypt: true};

        getFile('scores.json', options)
            .then((file) => {
                let scores = JSON.parse(file);
                if(scores !== null)
                this.setState({
                    scores: scores
                })
            })
    };

    /**
     * Save the users slider scores
     */
    saveScore = () => {
        const date = new Date();
        const idString = date.getFullYear() + '/' + date.getMonth() + '/' + date.getDate();
        const dateString = date.getMonth() + 1 + '/' + date.getDate() + '/' + date.getFullYear();
        let scores = this.state.scores;

        let score = {
            id: idString,
            date: dateString,
            anxiety: this.state.anxietySlider,
            depression: this.state.depressionSlider
        };

        scores.push(score);

        const options = {encrypt: true};
        putFile('scores.json', JSON.stringify(scores), options)
            .then(() => {
                this.setState({
                    scores: scores
                });
                this.loadGraphDataFromGaiaStorage();
            });
    };

    /**
     * Submits data to the local database
     */
    onSubmitData = () => {
        this.saveScore();
    };
    /**
     * On depression change the value of depressionSlider state changes
     * @param event change event
     * @param value change value
     */
    handleDepressionChange = (event, value) => {
        this.setState({depressionSlider: value});
    };
    /**
     * On Anxiety change the value of anxietySlider state changes
     * @param event change event
     * @param value change value
     */
    handleAnxietyChange = (event, value) => {
        this.setState({anxietySlider: value});
    };
    /**
     * Resets the sliders to 50
     */
    handleReset = () => {
        this.setState({anxietySlider: 50});
        this.setState({depressionSlider: 50});
    };

    render() {

        return (
            <div style={{
                padding: "2px"
            }}>

                <WmtpResultsGraph data={this.state.scores}
                                  graphHeight={300}
                                  dataKey="date"
                                  dataLines={[
                                      {
                                          "dataName": "anxiety",
                                          "lineName": "Anxiety Level",
                                          "strokeColor": "#0000cc"
                                      },
                                      {
                                          "dataName": "depression",
                                          "lineName": "Depression Level",
                                          "strokeColor": "#ff0800"
                                      }]}


                />
                <WmtpSlider
                    lowerTag={"Relaxed"}
                    upperTag={"Anxious"}
                    title="Anxiety"
                    value={this.state.anxietySlider}
                    handleChange={this.handleAnxietyChange}
                />
                <br/>
                <WmtpSlider
                    lowerTag={"Content"}
                    upperTag={"Depressed"}
                    title="Depression"
                    value={this.state.depressionSlider}
                    handleChange={this.handleDepressionChange}
                />
                <br/>
                <div style={{
                    textAlign: "center"
                }}>
                    <Button
                        label={"reset"}
                        onClick={this.handleReset}
                        style={{borderRadius: 2}}
                    >
                        Reset
                    </Button>
                    <Button
                        label={"submit"}
                        onClick={this.onSubmitData}
                        style={{borderRadius: 2}}
                    >
                        Submit
                    </Button>
                </div>
            </div>
        )
    };
}

export default Home;