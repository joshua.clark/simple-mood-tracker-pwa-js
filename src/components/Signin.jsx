import React, {Component} from 'react';
import {isUserSignedIn} from 'blockstack';
import AppBar from "@material-ui/core/es/AppBar/AppBar";

export default class Signin extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        const {handleSignIn} = this.props;

        return (
            <div className="panel-landing" id="section-1">
                <AppBar style ={{
                    textAlign: 'center'
                }}>
                    Mood Tracker PWA Login Screen
                </AppBar>
                <div style={{
                    textAlign: 'center'
                }}>
                    <h1 className="landing-heading">Welcome to Simple Mood Tracker PWA</h1>
                    <p className="lead">
                        <button
                            style={{
                                textAlign: 'center',
                                border: '1px solid black'
                            }}
                            className="btn btn-primary btn-lg"
                            id="signin-button"
                            onClick={handleSignIn.bind(this)}
                        >
                            Sign In with Blockstack
                        </button>
                    </p>
                </div>
            </div>
        );
    }
}