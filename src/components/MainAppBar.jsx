import React from 'react';
import AppBar from "@material-ui/core/AppBar/AppBar";
import Typography from "@material-ui/core/Typography/Typography";
import Toolbar from "@material-ui/core/Toolbar/Toolbar";
import {withStyles} from '@material-ui/core/styles';
import Button from "@material-ui/core/es/Button/Button";

const styles = {
    root: {
        flexGrow:1,
        position: "static",
    },
};
/**
 * Main App Bar
 */
class MainAppBar extends React.Component {

    render() {
        const {classes} = this.props;
        const {handleSignOut} = this.props;

        return (
            <div className={classes.root} >
                <AppBar position='static' >
                    <Toolbar>
                        <Typography variant="h6" color="inherit" style={{flex:1}}>
                            Simple Mood Tracker
                        </Typography>
                        <Button
                            color="inherit"
                            style={{
                                position: 'static',
                                marginRight: 2
                            }}
                            onClick={handleSignOut.bind(this)}
                        >
                            Logout
                        </Button>
                    </Toolbar>
                </AppBar>
            </div>
        )
    }
}
export default withStyles(styles)(MainAppBar);